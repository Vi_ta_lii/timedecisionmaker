//
//  Appointment.swift
//  TimeDecisionMaker
//
//  Created by Me on 5/15/19.
//

import Foundation

struct Appointment {
	
	let uid: String
	var title: String
	var startDate: Date
	var endDate: Date
}
