//
//  PrecedenceGroups.swift
//  TimeDecisionMaker
//
//  Created by Me on 5/16/19.
//

import Foundation

precedencegroup LeftFunctionApplicationPrecedence {
	associativity: left
	higherThan: RightFunctionApplicationPrecedence
}

precedencegroup RightFunctionApplicationPrecedence {
	associativity: right
	higherThan: AssignmentPrecedence
}
