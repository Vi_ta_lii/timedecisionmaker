//
//  Functions.swift
//  TimeDecisionMaker
//
//  Created by Me on 5/22/19.
//

import Foundation

func toString(_ cls: AnyClass) -> String {
	return String(describing: cls)
}
