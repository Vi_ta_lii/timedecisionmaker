//
//  StringKeys.swift
//  TimeDecisionMaker
//
//  Created by Me on 5/22/19.
//

import Foundation

struct Keys {
	
	struct NavigationItem {
		static let eventList = "Event list"
		static let calendars = "Calendars"
		static let freeSlots = "Free Slots"
	}
	
	struct FatalError {
		static let noInit = "init(coder:) has not been implemented"
	}
	
	struct Calendars {
		static let A = "A"
		static let B = "B"
		static let `extension` = "ics"
		static let allDay = "all day"
	}
	
	struct DateFormat {
		static let date = "dd.MM.YYYY"
		static let time = "HH:mm"
		static let distantFuture = " - To Infinity ... and Beyond!"
	}
	
	struct TestFail {
		static let noTestFiles = "Test files should exist"
		static let noAppointments = "At least one appointment should exist"
	}
	
	struct PermissionView {
		static let labelText = "This app needs calendar permission for proper work"
		static let buttonText = "Go to Settings"
	}
}
